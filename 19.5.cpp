﻿
#include <iostream>

class Animal
{
public:
    void Voice() const
    {
        Speak();
    }
    virtual void Speak() const = 0;

    virtual ~Animal() {}
};

class Cat : public Animal
{
public:
    virtual void Speak() const { std::cout << "Meow\n"; }

    ~Cat() { void Speak(); { std::cout << "Meow\n"; } };
};

class Dog : public Animal
{
public:
    virtual void Speak() const { std::cout << "Woof\n"; }

    ~Dog() { void Speak(); { std::cout << "Woof\n"; } };
};

class Bird : public Animal
{
public:
    virtual void Speak() const { std::cout << "Tweet\n"; }

    ~Bird() { void Speak(); { std::cout << "Tweet\n"; } };
};

class Frog : public Animal
{
public:
    virtual void Speak() const { std::cout << "Croak\n"; }

    ~Frog() { void Speak(); { std::cout << "Croak\n"; } };
};

int main()
{
    Animal * animals[4]{ new Cat, new Dog, new Bird, new Frog };

    for (Animal* a : animals)
    {
        a->Voice();
        delete a;
    }


    return EXIT_SUCCESS;
}